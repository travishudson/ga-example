import random
import string

# Generate the initial population
for i in range(8):
    for n in range(7):
        print("\"{}\",".format("".join([random.choice(string.ascii_lowercase) for k in range(6)])), end=" ")
    print()

