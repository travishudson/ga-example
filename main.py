#####################################################################
# Genetic Algorithm Example
# Originally written by Travis Hudson (January 27, 2015)
#####################################################################

import random
import string
from operator import itemgetter

# Set up the initial population with varied six character strings
# Does not need to be random, does not even need to be varied, just needs to exist
startingPopulation = [
"uzhqht", "lsivci", "vkdota", "jhtuzy", "arzzxq", "xsjqys", "bjtouz",
"uqxlsf", "isyoxr", "syshya", "civscf", "gbqcug", "klqjff", "wwceda",
"fkncqw", "nkyuvz", "njljwk", "cglpwq", "qfnsgn", "wbkbuo", "swnyvl",
"vumzgf", "zcpita", "ysyloc", "hcsyzw", "cccedz", "ciebfd", "kyllhm",
"obanhy", "mmqmbh", "kvucgq", "etykyb", "cpflcf", "khcrlc", "dfshjy",
"fjzehn", "zyxirs", "rmeelt", "skjdcy", "wwcnqw", "qfwwnf", "yzegvz",
"mtuwak", "wohgmu", "kiqita", "jyzbgi", "jyqmtf", "vpwfhu", "kjvgob",
"xckcuj", "rhpgqu", "lisyid", "qhfieh", "umokdk", "ickmyq", "rtgimi"]

#
# Return the fitness score of a given six character string, which is like DNA
#
def measureFitness(dna):
    result = 0
    result += dna.count("a")
    result += dna.count("b")
    result -= dna.count("q")
    result -= dna.count("x")
    return result

#
# Potentially mutate DNA. Might not do anything because of low mutation rate.
#
def mutate(dna):
    result = ""
    for i in range(len(dna)):

        # 0.2% change of mutation for each letter in the DNA sequence
        if random.randint(0, 1000) < 2:
            result += random.choice(string.ascii_lowercase)
        else:
            result += dna[i]

    return result

#
# Combine two sequences of DNA into one new sequence
#
def combineDNA(dna1, dna2):
    return random.choice([dna1[:len(dna1)//2] + dna2[len(dna2)//2:], dna2[:len(dna2)//2] + dna1[len(dna1)//2:]])

#
# Score the fitness level of the entire population, returning an array of two-member arrays,
# where the first item is the fitness score and the scond item is the DNA that was given that score
#
def scorePopulation(population):
    return [[measureFitness(dna), dna] for dna in population]

#
# Simulate evolution for the given population and number of generations
#
def evolve(population, generations):
    result = population[:]

    for i in range(generations):

        # assign fitness scores to each member and sort them from most fit to least fit
        scoredPopulation = sorted(scorePopulation(result), key=itemgetter(0), reverse=True)

        # remove the half of the population that got the lowest fitness scores
        scoredPopulation = scoredPopulation[:len(scoredPopulation)//2]

        # remove the scores; they aren't needed until the next round of scoring
        _, result = zip(*scoredPopulation)
        result = list(result)
        random.shuffle(result)

        # replace the missing half of the population with offspring from the most fit DNA
        newMembers = []
        for i, _ in enumerate(result):
            if i == len(result) - 1:
                newMembers.append(mutate(combineDNA(result[i], result[0])))
            else:
                newMembers.append(mutate(combineDNA(result[i], result[i+1])))
        result += newMembers

    return result

# Start of code execution. This chunk of code is run if this file is not being imported as a package.
if __name__ == "__main__":
    newPopulation = evolve(startingPopulation, 200)   # Run for 10 generations
    print(newPopulation)

